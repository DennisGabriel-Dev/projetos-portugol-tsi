programa //CADASTRO DE CLIENTE - DENNIS GABRIEL - 
{
	inclua biblioteca Util --> u
	inclua biblioteca Texto --> t
	inclua biblioteca ServicosWeb --> sw
	const inteiro tam = 4
	inteiro opcao, i , idCliente = 0, listaIdClientes[tam]
	cadeia nomeClientes[tam], enderecoClientes[tam] ,cpfClientes[tam], dataCadastro[tam] , primeirasLetras
	logico mantemPrograma= verdadeiro
	funcao inicio()
	{	
		
		enquanto(mantemPrograma){
			menu()
			leia(opcao)

			escolha(opcao){
			caso 1:
				cadastrarCliente()	
				pare
				
			caso 2:
				listarTodosClientes()
				pare

			caso 3:
				listarSomenteUmCliente()
				pare
			
			caso 4:
				buscarPorNome()
				pare

			caso 5:
				deletarCliente()
				pare

			caso 6:
				escreva("Tem certeza que deseja sair? (s/n) ")
				caracter sair
				leia(sair)
				se(sair == 's' ou sair == 'S'){
					escreva("Até a próxima! ")
					mantemPrograma = falso
					}
				pare
				
			caso contrario:
				escreva("Opção Inválida!")
				u.aguarde(2000)
				pare
			}
		}
	}

	funcao menu(){
		limpa()
		escreva("=======================================\n")
		escreva("=========== MENU DE CLIENTES ==========\n")
		escreva("=======================================\n")
		escreva("1 - Cadastrar Cliente\n")
		escreva("2 - Listar os dados de TODOS os Clientes\n")
		escreva("3 - Listar os dados de somente um Cliente\n")
		escreva("4 - Buscar por nome\n")
		escreva("5 - Deletar um cliente\n")
		escreva("6 - Sair do Programa\n\n")
		escreva("O que deseja fazer? ")
		
	}

	funcao cadastrarCliente(){
		limpa()
				escreva("===== CADASTRO DE CLIENTE =====\n")
				para(i = 0; i<u.numero_elementos(nomeClientes); i++){
					se(i == idCliente){
						escreva("Digite o nome do Cliente: ")
						leia(nomeClientes[i])
						t.caixa_alta(nomeClientes[i])
						escreva("Digite o CPF do Cliente: ")
						leia(cpfClientes[i])
						cadeia cpfTemp = cpfClientes[i]
						
						// Verifica se o CPF existe
						se(verificaCpfExistente(cpfTemp, idCliente) == verdadeiro){
							nomeClientes[idCliente] = ""
							cpfClientes[idCliente] = ""
							dataCadastro[idCliente] = ""
							enderecoClientes[idCliente] = ""
							//listaIdClientes[idCliente] = idCliente - 1
							escreva("O CPF informado já está cadastrado!\n")
							u.aguarde(2000)
							escreva("Por favor , tente novamente! Digite 1 para tentar novamente ou digite 5 para sair.")
							inteiro numeroSaida
							leia(numeroSaida)
							se(numeroSaida == 5){
								inicio()
							}
							limpa()
						}senao{
							escreva("Digite a Data de Cadastro do Cliente: ")
							leia(dataCadastro[i])
							escreva("Digite o CEP do Cliente: ")
							leia(enderecoClientes[i])
							listaIdClientes[i] = idCliente + 1
						}
							
						
						}
					}
				idCliente++
				escreva("CLIENTE CADASTRADO COM SUCESSO! ")
				u.aguarde(2000)
		}
		
	funcao listarTodosClientes(){
		limpa()
		para(i = 0 ;  i< tam ;  i++){
			se(nomeClientes[i] != ""){
					escreva("============================\n")
					escreva("\nNOME: "+ nomeClientes[i])
					escreva("\nCPF: " + cpfClientes[i])
					escreva("\nID CLIENTE: "+ listaIdClientes[i])
					escreva("\n============================\n")
				}
			}
			
		cadeia enter
		escreva("PRECIONE ENTER PARA SAIR: ")
		leia(enter)
		}

	funcao listarSomenteUmCliente(){
			limpa()
			cadeia cpfClienteIndividual
			escreva("DIGITE O CPF do Cliente:")
			leia(cpfClienteIndividual)
			para(i=0 ; i< u.numero_elementos(cpfClientes); i++){
				se(cpfClienteIndividual == cpfClientes[i]){
					escreva("ID CLIENTE: "+ listaIdClientes[i])
					escreva("\nNOME: "+ nomeClientes[i])
					//escreva("\nCPF: " + cpfClientes[i])
					escreva("\nDATA CADASTRO: " + dataCadastro[i])
					cadeia CEP = sw.obter_dados("https://viacep.com.br/ws/"+enderecoClientes[i]+"/json/")
					escreva("\nENDEREÇO: " + CEP)
					escreva("\n============================\n")
					pare
				}
			
			}
			cadeia enter
			escreva("PRESSIONE ENTER PARA SAIR: ")
			leia(enter)
	

	}

	funcao buscarPorNome(){
		escreva("Digite as três primeiras letras do nome do(s) Cliente(s):")
		leia(primeirasLetras)
		logico achouCliente = falso
		para(i= 0 ; i<tam ; i++){
			se (nomeClientes[i] != ""){
				se(t.extrair_subtexto(nomeClientes[i], 0, 3) == t.extrair_subtexto(primeirasLetras, 0, 3)){
						escreva("ID CLIENTE: "+ listaIdClientes[i])
						escreva("\nNOME: "+ nomeClientes[i])
						escreva("\nCPF: " + cpfClientes[i])
						escreva("\nDATA CADASTRO: " + dataCadastro[i])
						escreva("\nENDEREÇO: " + enderecoClientes[i])
						escreva("\n============================\n")
						achouCliente = verdadeiro
				}
			}
		}

		se(achouCliente == falso){
			escreva("Não existe cliente com as características informadas.")
			u.aguarde(2000)
			limpa()
		}
				
		cadeia enter
		escreva("PRECIONE ENTER PARA SAIR: ")
		leia(enter)
	}

	// funcao deletar Clientes
	funcao deletarCliente(){
		cadeia cpf
		escreva("Informe o CPF do Cliente: ")
		leia(cpf)
		se(verificaCpfExistente(cpf, tam)){
			para(i=0 ; i< tam; i++){
				se(cpf == cpfClientes[i]){
					cpfClientes[i] = ""
					nomeClientes[i]= "" 
					enderecoClientes[i] = ""
					dataCadastro[i] = ""
					escreva("CLIENTE DELETADO! :(")
					}
				}
		}
		senao{
			escreva("CPF inválido")
			}

		u.aguarde(2000)
		
	}

	// funcao testes

	funcao logico verificaCpfExistente(cadeia cpf, inteiro pos){
		para(i = 0 ;  i<pos; i++){
			se(cpf == cpfClientes[i]){
				retorne verdadeiro
				}
			}
		retorne falso
		}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 5466; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */