
programa
{
	inclua biblioteca Texto --> t
    
cadeia matCliente[4][4], nomeCliente, cpfCliente , enderecoCliente , cartaoDeCreditoCliente, es = ""
    inteiro i, j , id = 0, fix = 20
    caracter opcao = 's'
    funcao inicio()
    {
   	 enquanto(opcao == 's' ou opcao == 'S'){
   		 cadastrar()
   		 }
   		 mostrarTabela()
    }
    funcao cadastrar(){
    	
    	 limpa()
   	 escreva("-----CADASTRO DE CLIENTES----- \n")
   	 escreva("Nome do Cliente: ")
   	 leia(nomeCliente)
   	 escreva("Endereço: ")
   	 leia(enderecoCliente)
   	 escreva("CPF: ")
   	 leia(cpfCliente)
   	 escreva("Cartão de Crédito: ")
   	 leia(cartaoDeCreditoCliente)
   	 id ++
   	 
   	matCliente[0][0] = "Nome: "
    	matCliente[0][1] = "Endereço: "
    	matCliente[0][2] = "CPF: "	
    	matCliente[0][3] = "Cartão de Crédito: "
    	
   	 para(i=1; i<4 ; i++){
   		 para(j=0; j<4 ; j++){
   			   se(i==id){
   				 se(j==0){
   					 matCliente[i][j] = nomeCliente
   					 }
   				 senao se(j==1){
   					 matCliente[i][j] = enderecoCliente
   				 }senao se(j==2){
   					 matCliente[i][j] = cpfCliente
   				 }senao se(j==3){
   					 matCliente[i][j] = cartaoDeCreditoCliente
   				 }
   				 }
   			 }
   		 }
   	 	escreva("\nDeseja cadastrar outro Cliente? (s/n):")
   	 	leia(opcao)
   	 	
    }
   	 

    funcao mostrarTabela(){
    	limpa()
    	escreva("-----TABELA DE CLIENTES-----\n")
    	para(i=0;i<4; i++){
    		para(j=0; j<4; j++){
    			
    			se(i<3 e j<3){
    				escreva(matCliente[i][j],espaco(t.numero_caracteres(matCliente[i][j])),"|")
    				//escreva("\n"+matCliente[i][j])
    				}
 			//se(i==0 e j<4){escreva(matCliente[i][j])}
    			//escreva(matCliente[i][j]+espaco(t.numero_caracteres(matCliente[i][j]))+"|")
    			//se(j<4 e i<4){
    				//escreva(matCliente[i][j],espaco(t.numero_caracteres(matCliente[i][j])),"|")
    			//}
    			es = ""
    			 
    			}
    			escreva("\n--------------------\n")
    		}
	}

	funcao cadeia espaco(inteiro tamanho){
		para(inteiro z = 0 ; z<= fix - (tamanho-1); z++){
				es += " "
			}
		retorne es
		}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1468; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = {i, 7, 12, 1}-{j, 7, 15, 1}-{tamanho, 79, 30, 7}-{z, 80, 15, 1};
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */