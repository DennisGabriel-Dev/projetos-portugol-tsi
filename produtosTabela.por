programa
{
	inclua biblioteca Texto --> t
	cadeia produto[5][3] , nomeProduto, qtdProduto,  precoProduto
	inteiro i,j, fix = 15
	caracter opcao = 's'
	funcao inicio()
	{
		cadastrarProduto()
	}

	funcao cadastrarProduto(){
		produto[0][0] = "Nome"
		produto[0][1] = "Estoque" 
		produto[0][2] = "Preço"
		para(i=1; opcao == 's' ou opcao =='S' ; i++){		
			para(j=0; opcao == 's' ; j++){ 		
				escreva("Deseja cadastrar um produto (s/n): ")
				leia(opcao)
				enquanto(opcao == 's'){
					limpa()
					escreva("\nNome do Produto: ")
					leia(nomeProduto)
					produto[i][j] = nomeProduto
					
					escreva("Quantidade do produto: ")
					leia(qtdProduto)
					produto[i][j+1] = qtdProduto
					
					escreva("Preço do produto: ")
					leia(precoProduto)
					produto[i][j+2] = precoProduto
					
					escreva("\nDeseja cadastrar outro produto (s/n): ")
					leia(opcao)
					i++
					}
				}
			}
		
		escreva("\n\n")
		
		escreva("Tabela de produtos: \n---------------------\n")
		para(i=1; i<5 ; i++)
		{		
			para(j=0; j<3 ; j++)
			{
				escreva(produto[0][j] + ": " + produto[i][j]+"\n")
				}
				escreva("---------------------\n")
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 497; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */