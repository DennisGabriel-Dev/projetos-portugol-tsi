programa
{
	inclua biblioteca Tipos --> t
	cadeia matCliente[5][4], matProduto[5][4],nomeCliente, cpfCliente , enderecoCliente , cartaoDeCreditoCliente
	cadeia nomeDoProduto, qtdProduto 
     inteiro i, j , id = 0, opcaoMenu, idProduto = 0
     caracter opcao = 's'

	//MatProduto
	real precoProduto
     //Funcao venda
     inteiro idClienteVenda,  idProdutoVenda
     real valorVenda = 0.0,  qtdProdutos
	funcao inicio()
	{
	valoresBase()

	enquanto(verdadeiro){
	
	opcao = 's'
	escreva("****LOJA VIRTUAL****\n1 - Cadastrar Cliente \n2 - Tabela Cliente \n3 - Cadastrar Produto \n4 - Tabela Produtos \n5 - Venda \n\nDigite a opção desejada:")
	leia(opcaoMenu)
	
	escolha(opcaoMenu){
		caso 1:
			enquanto(opcao == 's' ou opcao == 'S'){
   		 	cadastrarCliente()
   		 	}
   		     pare
		caso 2:
   		 	mostrarTabelaCliente()
   		 	pare
   		caso 3:
   		 	enquanto(opcao == 's' ou opcao == 'S'){
   		 		cadastrarProduto()
   		 	}
   		 	pare
   		 caso 4:
   		 	mostrarTabelaProduto()
			pare

		caso 5:
			venda()
			pare
		}
		}
   		 
    }
    funcao cadastrarCliente(){
    	 limpa()
   	 escreva("-----CADASTRO DE CLIENTES----- \n")
   	 escreva("Nome do Cliente: ")
   	 leia(nomeCliente)
   	 escreva("Endereço: ")
   	 leia(enderecoCliente)
   	 escreva("CPF: ")
   	 leia(cpfCliente)
   	 escreva("Cartão de Crédito: ")
   	 leia(cartaoDeCreditoCliente)
   	 id ++
   	 para(i=1; i<5 ; i++){
   		 para(j=0; j<4 ; j++){
   			   se(i==id){
   				 se(j==0){
   					 matCliente[i][j] = nomeCliente
   					 }
   				 senao se(j==1){
   					 matCliente[i][j] = enderecoCliente
   				 }senao se(j==2){
   					 matCliente[i][j] = cpfCliente
   				 }senao se(j==3){
   					 matCliente[i][j] = cartaoDeCreditoCliente
   				 }
   				 }
   			 }
   		 }
   	 	escreva("\nDeseja cadastrar outro Cliente? (s/n):")
   	 	leia(opcao)
   	 	
    }
   	 

    funcao mostrarTabelaCliente(){
    	limpa()
    	escreva("-----TABELA DE CLIENTES-----\n")
    	para(i=1;i<5; i++){
    		para(j=0; j<4; j++){
    			escreva(matCliente[0][j] + matCliente[i][j]+"\n")
    			}
    			escreva("----------------\n")
    		}
	}

	funcao cadastrarProduto(){
	
		limpa()
		escreva("-----CADASTRO DE PRODUTOS-----\n") 
		enquanto(opcao=='s'){
			idProduto++
			para(i=1;i<5;i++){
	  			para(j=0; j<4; j++){
	  				se(i == idProduto){
	  					se(j != 2){
			  				escreva(matProduto[0][j])
			  				leia(matProduto[i][j])
		  						}
	  					}
	  				}
	  			}
	  				
					
	  				colocaIdProdutos(idProduto)
	  				


	  		
					escreva("\nDeseja cadastrar outro produto? (s/n): ")
					leia(opcao)
					se(opcao =='n'){
						pare
						}
			
	  	}
	}

	  	funcao mostrarTabelaProduto(){
	  		limpa()
    			escreva("-----TABELA DE PRODUTOS-----\n")
	  		para(i=1;i<5;i++){
	  			para(j=0; j<4; j++){
	  				escreva(matProduto[0][j]+matProduto[i][j]+"\n")	
	  				}
	  				escreva("\n--------------------\n")
	  			}
	  		}

	  	funcao valoresBase(){
	  		//Valores Padrões da Matriz "Cliente"
			matCliente[0][0] = "Nome: "
		    	matCliente[0][1] = "Endereço: "
		    	matCliente[0][2] = "CPF: "	
		    	matCliente[0][3] = "Cartão de Crédito: "

	  		//Valores Padrões da Matriz "Produto"
	  		matProduto[0][0] = "Nome do produto: "
			matProduto[0][1] = "Quantidade: "
			matProduto[0][2] = "ID: "
			matProduto[0][3] = "Preço: "
	  		}
	  	funcao colocaIdProdutos(inteiro numero){
	  			//ID PRODUTO
					para(i=1;i<5;i++){
	  					para(j=0; j<4; j++){
	  						se(i == numero){
	  							se(j==2){
	  								matProduto[i][2] = t.inteiro_para_cadeia(numero,10)
	  							}
	  						}
	  			
	  					}
	  				}

	  			//
	  		}

	  	funcao venda(){
	  		/*
	  		escreva("Digite o ID do Cliente: ")
	  		leia(idClienteVenda)
			escreva("\n-----------------")
	
	  		para(i=1; i<5; i++){
	  			para(j=0; j<4; j++){
	  				
	  				se(i==idClienteVenda)escreva(matCliente[0][j]+ matCliente[i][j]+"\n")	
	  					
	  				}
	  			}*/
	  		logico desejoVenda = verdadeiro
			enquanto(desejoVenda == verdadeiro){
			caracter desejoVendaChar
			escreva("Deseja vender? (s/n)")
	  		leia(desejoVendaChar)
	  		se(desejoVendaChar == 'n' ou desejoVendaChar == 'N'){
	  			desejoVenda = falso
	  			}
	  		escreva("\nDigite o(s) ID(s) da compra: ")
	  		leia(idProdutoVenda)
	  		escreva("Digite a quantidade: ")
	  		leia(qtdProdutos)
			inteiro qtdEstoque
	  		

	  		para(i=1; i<5; i++){
	  			para(j=0; j<4; j++){
	  				
	  				se(i==idProdutoVenda){
	  					escreva(matProduto[0][j]+ matProduto[i][j]+"\n")
	  					valorVenda += qtdProdutos * t.cadeia_para_real(matProduto[i][3])
	  					qtdEstoque = t.cadeia_para_inteiro(matProduto[i][1],10) - qtdProdutos
	  					se(qtdProdutos>qtdEstoque){
	  						escreva("QUANTIDADE ACIMA DO PERMITIDO, o estoque atual é: "+ qtdEstoque)
	  						escreva("Por favor, tente novamente. Ou verifique o estoque na opção 4, no menu principal.")
	  			}
	  					 matProduto[i][1] = t.inteiro_para_cadeia(qtdEstoque,10) 
	  					escreva("\nO valor da venda total: " + valorVenda+"\n")
	  					escreva("Estoque atual é de: "+ qtdEstoque+"\n")
	  					pare
	  					}
	  				}
	  			}
				}
			
	  		}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 4870; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */