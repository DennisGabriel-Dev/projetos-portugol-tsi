programa
{
	inclua biblioteca Util --> u
	cadeia nomes[10] 
	inteiro i, nomeSorteado
	
	funcao inicio()
	{	
		para(i = 0 ; i< u.numero_elementos(nomes); i++){
			cadeia nome
			escreva("Digite o " + (i+1)+ "o nome: ")
			leia(nome)
			nomes[i] = nome
			}
			nomeSorteado = u.sorteia(0, u.numero_elementos(nomes))
			escreva("\n\nO nome sorteado foi: " + nomes[nomeSorteado] + "\n\n\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 205; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */